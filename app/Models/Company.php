<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

class Company extends Model
{
    use HasFactory;

    /**
     * Mass-assignable attributes.
     * 
     * @var array
     */
    protected $fillable = [
        'name',
        'market_id',
        'type_id',
        'number',
        'entity',
        'market',
        'type',
    ];

    /**
     * Get company from it's entity ID.
     * 
     * @param string $entity
     * @return Company
     */
    public static function entity(string $entity): Company
    {
        return self::where('entity', $entity)->firstOrFail();
    }

    /**
     * Company market.
     * 
     * @return BelongsTo
     */
    public function market(): BelongsTo
    {
        return $this->belongsTo(Market::class);
    }

    /**
     * Company type.
     * 
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * Set company market by name (country).
     * 
     * @param string $name
     * @return void
     */
    public function setMarketAttribute(string $name): void
    {
        $this->attributes['market_id'] = Market::where('name', ucfirst($name))->first()->id;
    }

    /**
     * Set company type by name (small, medium, large).
     * 
     * @param string $name
     * @return void
     */
    public function setTypeAttribute(string $name): void
    {
        $this->attributes['type_id'] = Type::where('name', strtolower($name))->first()->id;
    }

    /**
     * Get company scheduled based on market and type combination.
     * 
     * @return Collection
     */
    public function schedule(): Collection
    {
        $schedule = Schedule::where('market_id', $this->market_id)
            ->where('type_id', $this->type_id);

        if ($schedule->count() === 0) {
            return collect([]);
        }

        return collect($schedule->first()->days);
    }
}
