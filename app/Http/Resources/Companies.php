<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Companies extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'companyId' => $this->entity,
            'market' => $this->market->name,
            'type' => $this->type->name,
        ];
    }
}
