<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Company extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'companyId' => $this->entity,
            'notifications' => $this->schedule()->map(function($day) {
                return now()->day($day + 1)->format('d/m/Y');
            }),
        ];
    }
}
