<?php

namespace Tests\Feature;

use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CompaniesTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Company::factory()->count(10)->create();
        
    }

    public function test_can_index()
    {
        $response = $this->get('/companies');

        $response->assertStatus(200);
    }

    public function test_index_structure()
    {
        $response = $this->get('/companies');

        $response->assertJsonStructure([
            'data' => [
                [
                    'companyId',
                    'market',
                    'type',
                ]
            ]
        ]);
    }

    public function test_can_show()
    {
        $response = $this->get('/companies/'. Company::first()->entity);

        $response->assertStatus(200);
    }

    public function test_show_structure()
    {
        $response = $this->get('/companies/'. Company::first()->entity);

        $response->assertJsonStructure([
            'data' => [
                'companyId',
                'notifications',
            ]
        ]);
    }

    public function test_create_company()
    {
        $response = $this->post('/companies', [
            'name' => 'Test Company',
            'market' => 'denmark',
            'type' => 'small',
            'number' => '0123456789',
            'entity' => 'aad7a630- af1c-4952-9cb4-44b8b847853b',
        ]);

        $response->assertStatus(200);
    }
}
