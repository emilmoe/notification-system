<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\Market;
use App\Models\Type;
use Illuminate\Database\Eloquent\Factories\Factory;
use Webpatser\Uuid\Uuid;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'market_id' => Market::inRandomOrder()->first()->id,
            'type_id' => Type::inRandomOrder()->first()->id,
            'number' => $this->faker->numerify('##########'),
            'entity' => Uuid::generate(),
        ];
    }
}
