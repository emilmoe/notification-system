<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Market;
use App\Models\Schedule;
use App\Models\Type;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Market::updateOrCreate(['name' => 'Denmark']);
        Market::updateOrCreate(['name' => 'Norway']);
        Market::updateOrCreate(['name' => 'Sweden']);
        Market::updateOrCreate(['name' => 'Finland']);

        Type::updateOrCreate(['name' => 'small']);
        Type::updateOrCreate(['name' => 'medium']);
        Type::updateOrCreate(['name' => 'large']);

        collect(['small', 'medium', 'large'])->each(function($type) {
            Schedule::updateOrCreate([
                'market_id' => Market::where('name', 'Denmark')->first()->id,
                'type_id' => Type::where('name', $type)->first()->id,
            ], [
                'days' => [1,5,10,15,20]
            ]);
        });

        collect(['small', 'medium', 'large'])->each(function($type) {
            Schedule::updateOrCreate([
                'market_id' => Market::where('name', 'Norway')->first()->id,
                'type_id' => Type::where('name', $type)->first()->id,
            ], [
                'days' => [1,5,10,20]
            ]);
        });

        collect(['small', 'medium'])->each(function($type) {
            Schedule::updateOrCreate([
                'market_id' => Market::where('name', 'Sweden')->first()->id,
                'type_id' => Type::where('name', $type)->first()->id,
            ], [
                'days' => [1,7,14,28]
            ]);
        });

        Schedule::updateOrCreate([
            'market_id' => Market::where('name', 'Finland')->first()->id,
            'type_id' => Type::where('name', 'large')->first()->id,
        ], [
            'days' => [1,5,10,15,20]
        ]);
    }
}
